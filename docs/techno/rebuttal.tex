  \documentclass[12pt]{article}
\usepackage{fullpage,hyperref,natbib,amsmath,amssymb, amsthm}
\usepackage{import, color,graphicx}

\usepackage{fullpage}
\usepackage{epsf}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{setspace}
\usepackage[boxed]{algorithm}
\usepackage{algpseudocode}
\usepackage{algorithmicx}
\usepackage[usenames, dvipsnames]{xcolor}

\usepackage{caption}
\usepackage{subcaption}
\usepackage[normalem]{ulem}
\usepackage{csquotes}

\onehalfspacing

\newtheorem{Proposition}{Proposition}[section]

\DeclareMathOperator{\Diag}{Diag}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator\argmin{arg min}


\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}


\title{Responses to Reviewers' Comments}
\date{}
\author{}


\begin{document}
\maketitle

\vspace{-1.5cm}

\noindent Manuscript ID: \textbf{TCH-18-177}\\
Paper: Distance-distributed design for Gaussian process surrogates

\bigskip
\emph{Many thanks to the editorial team for the thoughtful comments in their
reports.  They are indeed much appreciated.  We have carefully addressed each
one in our revised manuscript. In particular, we extended our empirical work
out to six dimensions (adding $d=5$ and $d=6$), which revealed an inefficiency
in our betadist optimization setup.  Fixing that ``bug'' led to improvements
in all dimensions, so we are grateful to the editorial team for encouraging us
to better stress our methodology and empirical work. Consequently, we believe
that the revised manuscript is much improved and hope that you will agree.
Below, responses are in italics.  In the manuscript, added text is highligted
in blue, with the exception of small typo fixes. There were many cuts to
streamline the presentation and correct the exposition, but these are not
highlighted.}



\section*{Response to Editor}
\begin{enumerate}
\item  Many (most, in my experience) response surfaces from deterministic
computer experiments of real physical systems are quite smooth, at least over
most of the viable design space of interest. If you are pretty certain in
advance that your response surface is relatively smooth (i.e., for Gaussian
correlation, you know that $\theta$ is relatively large), then you probably do
not need any closely-spaced points to estimate $\theta$, and your conclusions
might change. You essentially dismiss situations like this in the beginning of
Section 2.2, by claiming prior knowledge of $\theta$ is rare. But the
situation I mention above is actually quite common, at least for modeling of
real physical systems. I don't think it would be too hard for users to specify
some small $\delta$, such that the correlation between the responses $y(x)$
and $y(x^{'})$ should be above some value (e.g., 0.95) for $||x-x^{'}|| <
\delta$. This could be translated to a lower bound on $\theta$. Could your
approach be modified for these situations, or would the conclusions on what
constitutes good distance distributions change if we only cared about larger
$\theta$ situations? Some discussion of this is warranted.

$\rightarrow$ {\em Thanks, good point! We generally agree, especially when
simulations come from industrial engineering applications.  In
machine/reinforcement learning (e.g., robotics), fluid dynamics and materials
science applications, we might argue for a more nuanced characterization with
wiggle room for a bit more ``surprise'' on the lengthscale front.
Nevertheless, we have added a paragraph to our discussion that addresses this
very typical situation.  We see no reason why our methodology could not be
applied equally to narrower $\theta$ ranges -- simply swap $\theta \in
(\varepsilon, \sqrt{d})$ for something else and ``go'' -- but speculate that
restrictions to subsets in the upper end of that range would probably deminish
the value of our approach relative to space-filling alternatives. }

\item One of the authors (Peihuang Lu) is listed incorrectly in the Byrd, et al citation. 

$\rightarrow$ {\em Thanks! This has been corrected.}


\item  Regarding the comment at the bottom of page 7, that the RMSE increases
as $\theta$ increases, and that this implies that it is harder to accurately
estimate lengthscales as they become longer. I am not sure this is a good way
to interpret it. What counts (in terms of how much errors in estimating
$\theta$ degrade the response prediction) is probably closer to the relative
error, i.e., the error divided by the true $\theta$.  If you changed the
vertical axes in Fig. 1a to report RMSE/$\theta_{true}$, it would probably be
a better reflection of how hard it is to estimate the lengthscale parameters.
Then you might not need to plot the ``standardized" results in Fig 1b.

$\rightarrow$ {\em Thanks a lot for the suggestion. We tried
the RMSE/$\theta_{true}$ transformation, but it doesn't work well. 
\begin{figure}[ht!]
  \centering
  \includegraphics[scale=0.6]{rmse_over_theta.pdf}
  \caption{RMSE/$\theta_{true}$ v.s. $\theta_{true}$}
  \label{fig:rmse_over_theta}
\end{figure}
The results of
this transformation for dimension 2 and 4 are displayed in Figure
\ref{fig:rmse_over_theta} in this rebuttal (we did not promote it to the
manuscript). Observe that the transformation is insufficient to eliminate
trend in the mean and variance. However, this study -- in addition to expansion
to higher dimensions -- did lead to upgrades in how we center and scale our
accuracy metric in $\theta$, separately for viewing (Figures 1--2) and
optimization (Figures 4--5) purposes.  In particular, we now model log MSE as
opposed to square root MSE (i.e., RMSE). 
\begin{figure}[ht!]
  \centering
  \includegraphics[scale=0.6]{../paper/theta2d.pdf}
  \includegraphics[scale=0.6]{../paper/theta5d.pdf}
  \caption{RMSEs from design experiment and de-trending surface.}
\label{fig:rmses}
\end{figure}
For example, as shown below in Figure
\ref{fig:rmses} (also see revised Figure 1 in the manuscript), the {\tt hetTP}
fit to such data adequately addresses both concerns (detrending both mean and
variance), leveling the playing field for a clear, automatable, fair
comparison between design methods.  We still think its ``harder'' to estimate
larger $\theta$ values, at least when accuracy is estimated by MSE, but now
see that as a bit of a red herring and qualify as such in the revised text.}

\end{enumerate}

\pagebreak


\section*{Response to the Associate Editor}

This paper introduced a new class of design called distance-based designs for computer
experiments. The idea is to approximate the pairwise distances between design points by
a beta distribution with unknown shape parameters. Although the idea of constructing de-
signs by a distance-based distribution seems to be interesting, there are several deficiencies
in the manuscript that prevent me from fully comprehending the results. I have several
major concerns which are given below.

\bigskip
\noindent \textbf{Major Comments}:

\begin{enumerate}
\item If I understand correctly, the maximin criterion authors referred to is
the original one proposed by Johnson et al.~(1990) which indeed focuses too
much on the extreme cases. However, the $\phi_p$ criterion (Morris and
Mitchell 1995) that commonly used to search for maximin designs incorporates
all the pairwise distances. I am wondering how the optimal maximin designs
obtained by $\phi_p$ compare with the proposed designs and random designs.

$\rightarrow$ {\em We really appreciate this point, thanks. We have added some
description of the $\phi_p$ criteria in the intro and also added it as a
comparator ($p=2$) in our empirical work. Actually, it performs worse than
maximin when estimating $\theta$.  Morris and Mitchell explain that there is a
lack of uniqueness in maximin designs and that $\phi_p$ are maximin designs
for all $p$, in the sense of maximizing minimum distance, although the
equivalence is most direct when $p \rightarrow \infty$. We chose $p=2$ in
order to have the $\phi_p$ design least like ordinary maximin, i.e., one which
seeks spread in other distances beyond just the minimum.  Spreading those
distances out turns out to make a bad situation even worse.}

\item It is not clear to me that why do we focus on the best 50 and worst 50
of random samples in Figure 3. Why not look at the distribution of the random
sample? The implication is not clear to me. Furthermore, there is clear
explanation that beta distribution is the best fit. Why not other
distributions? Is this distributional assumption robust to high-dimensional
input space?

$\rightarrow$ {\em Thanks for the comment. Any individual random design could
have points anywhere, and thus exhibit any pairwise distance distribution.
Sometimes they might look like LHS; sometimes like maximin, usually like
something else altogether, etc. Our aim here was to show that the best random
designs don't look like maximin, or like unifdist, from the perspective of
pairwise distances.  (We chose the 50 best, but that's arbitrary.) Instead they
look a little like a beta distribution, unimodal over the distances of
interest, which surprised us when we first saw it and inspired that choice
(i.e., of Beta) in our methodological work and optimizations.  Clearly this is
also arbitrary, and we say so (now more prominently) in our Section 6
discussion, but it works well. In our revision we have removed the ``worst
50'' curve as it was a bit of a distraction -- we have no interest in making
things bad. Our reworked Figure 5 already serves to make clear where the
sub-optimial (and pathologically bad) Beta distributions are.}

\item The choice of beta distribution seems to be specific to the unknown
lengthscales. The authors mentioned that the proposed method should be able to
extended to separable Gaussian correlation, however, it is not clear to me how
this can be extended. For example, a common choice is to have one correlation
parameter for each input and therefore $\theta$ becomes a $d$-dimensional
vector. We might be able to extend the proposed idea by modifying the RIMSE,
but it is not clear to me what will be the distribution in this case. Can beta
distribution with only two parameters still be a good approximation no matter
what $d$ is?

$\rightarrow$ {\em Thanks -- a valid point. Product form/separable kernels are
common and we only treat the simpler isotropic case.  We haven't played with
anything more general extensively.  A single beta distribution with only one
set parameters, as you suggest (and think you have guessed), seems unlikely to
work well.  (We haven't had any success.)  However, the fact that the product
form measures correlation separately in each axis direction suggests that
separate, 1d applications of the betadist idea might be one way forward: i.e.,
a separate betadist design in each coordinate. This is easy to say, but much
harder to check if it's a good idea.  We plan to investigate as part of our
future/thesis work. In our revised Section 6 discussion we have commented on
ideas to this effect.}

\item I am confused about the next step after the estimation of $\alpha$ and
$\beta$ by minimizing RIMSE. If I understand correctly, optimal designs could
be found by matching the corresponding pairwise distance distribution with the
betadist($\hat{\alpha}$, $\hat{\beta}$). If this is the case, I am wondering
if such optimal designs uniquely exist?

$\rightarrow${\em Thanks, and your description of the steps is correct
although we might quibble a bit with the verbiage. Like LHSs, betadist designs
are random designs, so there is no sense of uniqueness in the typical sense.
But betadist designs are parameterized by $\alpha$ and $\beta$, and some of
those choices are better than others.  Are the optimal choices unique?  We
don't know -- it's doubtful -- but we can find good settings and show, as we
do in Figure 5, that there is a degree of robustness in ``nearby'' choices of
those parameters.  Another degree of non-uniqueness comes from the fact that
betadist designs only care about relative, pairwise distance not position.
There are many ways (uncountably many?) you could move design points around
without changing the distribution of pairwise distances therein.  This is the
reason we introduced lhsbeta in Section 4, to lean on a secondary criteria
(one-dimensional uniformity) in order to better control position in addition
to relative distance(s).  No single change in the revised manuscript targets
such clarifications directly, but we have worked to find economies and
opportunities to better explain our motivation and methods and make our
presentation more concise (minor comment below).}


\end{enumerate}

\bigskip\bigskip
\noindent \textbf{Minor Comments}:

\begin{enumerate}
\item I would recommend rewriting/shortening the paper so that it is more concise.

$\rightarrow$ {\em Indeed, we have gone carefully through the paper to look for
economies.  We have cut several passages outright, and found bloat to
cut/reword in others. The AE will notice that the revised version is about the
same length as the previous one, however, since we were encouraged to expand
our empirical work into higher dimensions and add other discussions. }

\item Figure 1 is hard to read because the big dots (maximin) and small dots (unifdist) are
quite similar to each other. There should be some other ways to demonstrate this,
such as using different numbers instead of shapes.

$\rightarrow$ {\em Thanks, we have replaced the small dots with star shapes.}

\item Page 8: line 42 ``best v.~second best'' should be ``best vs.~second
best''. There are some other typos and grammatical errors throughout the
paper. Please double check carefully.

$\rightarrow$ {\em Done (to the best of our ability), thanks!}

\end{enumerate}


\pagebreak

\section*{Response to Reviewer \#1}

The paper shows that the designs based on commonly used space-filling design
can perform poorly when the GP is to be used with unknown (hyper) parameters.
Also proposed are the numerical methods that can provide designs that have
better properties in terms of distance distribution. Designs with superior
properties in distance distribution are shown to outperform the conventional
space-filling designs.

\bigskip
\medskip
\noindent \textbf{Major Comments}:

\noindent 
\begin{enumerate}
\item The authors point out an important issue in designs for computer
experiments. Space-filling designs have been studied for several decades, and
(very) broadly speaking there exist two branches of research – designs for
integration such as LHS, and designs for modeling, or model-based designs. An
important lacking point in model-based designs is that the involved parameters
are unknown. Not suprisingly when the design is poor when produced with a bad
choice of parameters. This work seems to have investigated such issues in a
very practical manner. The paper is well written (except a few lapses in
writing). It gets its task done, as opposed to many of the research papers
present lengthy results that cannot be used in practice. However the paper is
lacking an important discussion point; the distance-based approach is very
closely related to the variogram approach in spatial statistics as the authors
already mentioned that “GP surrogates are fundamentally the same kriging from
the spatial statistics literature.” The effect of distance distribution has
been studied in spatial statistics literature as well, for example, see the
\href{https://www.jstor.org/stable/1391002?seq=1#metadata_info_tab_contents}{link}.
Although the literature in the link is not directly related to the submitted
manuscript, there have been studies in this spirit. I personally don't like
the variogram approach, but it may be worth spending some time to look into
those. It will provide a better insight regarding why some designs perform
better than the other for $\theta$ in RMSE. Having some connection to this
work would make this work more valuable.

$\rightarrow$  {\em  Thanks, this is an excellent point! We have expanded the
introduction to include that reference and a brief discussion about how we see
it as complimentary to our proposed methodology. }

\end{enumerate}

\bigskip\bigskip
\noindent \textbf{Minor Comments}:

\begin{enumerate}
\item page 1 Line 46. The number of runs?

$\rightarrow$ {\em Thanks, fixed! }

\item page 2 Line 15. Chen et al (2016)?

$\rightarrow$ {\em Thanks, fixed!}

\item page 3 Line 12. Do you mean Maximum entropy design by “ME design”?

$\rightarrow$ {\em Thanks, we have expanded all ME design references to
maximum entropy design.}

\item page 3 Line 19. “A clumbsy choice of either is bad news”: typo and the
sentence sounds too informal and unclear.

$\rightarrow$ {\em Thanks, we have deleted this sentence.}

\item page 5 Line 53. This is a bit unclear and given without much context. Is
it related to ``In many cases, the only functions of the parameters that are
important are those that determine the behavior of the covariance function (or
variogram) at the origin''?

$\rightarrow$ {\em Sorry, we're a little puzzled by this quote, as it
doesn't appear to be taken from our manuscript.  All we're trying to say here
is that if you don't observe any squared distances of, say $0.1$ or smaller in
your design, then your likelihood can't support $\theta < 0.1$, except perhaps
as an awkward encoding of lack of any signal, if the data seem to support that
hypothesis.  Of course, this is intuition, which is not material to the
substance of our methodological contribution -- although it did inspire it.}

\item page 6 Line 8. “. . . in nature”. $\rightarrow$ “. . . in nature.”

$\rightarrow$ {\em Thanks! Nice catch!}

\item page 6 footnote: I don't think footnote is needed here. Put it in the main text.

$\rightarrow$ {\em Thanks, we have put it in the main text.}

\item page 8 Fig 1: Use better label for figures.

$\rightarrow$ {\em Thanks, we have changed the x-axis label to ``true theta".}

\item page 9 Fig 2: Clean up the table on the bottom right. It would be read better with less digits.

$\rightarrow$ {\em Thanks, they have been reduced to three digits now!}

\item page 10 Algorithm 1: Isn't the algorithm getting stuck in the KSD part?
Can you provide a bit more detail here?

$\rightarrow$ {\em Sure. Sometimes, especially when $n$ is large or the input
dimension $d$ is high, the algorithm may make little progress after a large
number of random searches, having converged to a local rather than global
optima. This is the drawback of a stochastic, greedy search algorithm. We have
added a sentence in our revision clarifying this aspect but also commenting
that we find that nevertheless the empirical distribution of distances it
finds are very close to $F$, and we have found little value in random
restarts, say, in search of more global optima.  Our empirical results with
unifdist and betadist designs are a testament to this good behavior.}

\item page 10 line 51: comment on ks.test customization can be moved to the appendix.

$\rightarrow$ {\em Thanks. Because we don't have an appendix for this paper, and
it seems silly to create one for just two sentences, we have decided to cut
that text.}

\item page 11 Fig 3: what is $x$ and $y$?

$\rightarrow$ {\em Thanks, we have fixed the axis labels in the figure and
explain that $x$ is pairwise distance and  $y$ is the empirical (kernel)
density.}

\item page 12 line 12-17: this is confusing. Please rewrite.

$\rightarrow$ {\em Many thanks.  We have reworded some of the sentences here
in hopes of making things more clear.  We're trying to foreshadow some of the
work in the following Section 3.2, and indicate a certain robustness to the
choice of $\alpha$ and $\beta$, within reason.}

\item page 12 line 38: Specify what exactly you mean by $X^{(t)} \sim
\mathrm{betadist}_{n,d}(\alpha, \beta)$.

$\rightarrow$ {\em Thanks.  We have entirely re-worked this section in response to 
Referee 2's request for higher dimensional examples.  As part of that rework we 
have added clarification about the $\mathrm{betadist}_{n,d}(\alpha, \beta)$ notation.}

\item page 13 line 49: need not be $\rightarrow$ needs not be

$\rightarrow$ {\em Fixed, thanks!}

\item page 14 line 37-52: this can be moved to the appendix (along with the footnote.)

$\rightarrow$ {\em Thanks.  We agree that this discussion is totally
tangential.  Since we do not have an appendix, we have decided to cut that
paragraph in its entirety. }

\item page 22 Tab 1: Clean up the table on the bottom right. It would be read
better with less digits.

$\rightarrow$ {\em Thanks.  We have now simplified the scientific notation in
all tables in Section 5.2. }

\item page 24 line 29: I believe the repository will be specified after the
review process.

$\rightarrow$ {\em Yes, absolutely.  And the Editor has access to an
unblinded version.}

\end{enumerate}



\medskip 


\section*{Response to Reviewer \#2}

The authors show that purely random design is superior to higher-powered
alternatives. They study the distribution of pairwise distances between design
elements and develop a numerical scheme to optimize those distances for a
given sample size and dimension. They illustrate how the distance-based
designs and their hybrids with more conventional space-filling schemes,
outperform in both static and sequential settings. However, their conclusion
is based on some limited simulations (e.g. small $n$ and $d = 2,3,4$ only). There
is no theoretical result to support their finding. My major concern is that
their conclusion may not be always true for general cases.

\medskip
\noindent $\rightarrow$ {\em Thanks.  You are correct that there is no theory.
But we do provide many concrete examples which show that theoretically sound
design strategies like maximin, $\phi_p$ and LHS lead to very poor results in
practice and we think that's valuable.  These shortcomings are easy to correct
in some representative cases, which we illustrate.

The contribution is highly empirical, and we welcome the opportunity to expand
upon those efforts, particularly as regards input dimension.  We have
augmented with $d=5$ and $d=6$ in our revision, showing that the results in
those cases are in line with our previous, more limited experimental campaign.
Expanding to high dimensions did reveal some inefficiencies in our setup,
particularly regarding the Bayesian optimization of $\alpha$ and $\beta$
parameters.  Correcting these led to improved results across the board, so we
thank the referee for that. }


\end{document} 
