library(lhs)
library(laGP)
library(doParallel)
library(foreach)
source("../design/maximin.R")
source("../design/ud_new.R")
source("../design/lhsbeta_design.R")
eps <- sqrt(.Machine$double.eps)


## Griewank function, the target function to be minimized
f <- function(X, scale=5){
  if(is.null(nrow(X))) X <- matrix(X, nrow=1)
  X <- scale * (X - 0.5)
  f <- rowSums(X^2)/4000 - apply(cos(t(X)/sqrt(1:ncol(X))), 2, prod) +1
  return(f)
}
## calculate EI
EI <- function(gpi, x, fmin, pred=predGP)
{
  if(is.null(nrow(x))) x <- matrix(x, nrow=1)
  p <- pred(gpi, x, lite=TRUE)
  d <- fmin - p$mean
  if(p$s2 <= 0){
    sigma <- eps
  }else{
    sigma <- sqrt(p$s2)
  }
  dn <- d/sigma
  ei <- d*pnorm(dn) + sigma*dnorm(dn)
  return(ei)
}

## maximize EI = minimize obj.EI
obj.EI <- function(x, fmin, gpi) - EI(gpi, x, fmin)

## find the location with maximum EI in the design space
EI.search <- function(X, y, gpi, multi.start=5)
{
  m <- which.min(y)
  fmin <- y[m]
  start <- matrix(X[m,], nrow=1)
  if(multi.start > 1) 
    start <- rbind(start, randomLHS(multi.start-1, ncol(X)))# start = current fmin location and multi.start-1 points from LHS design
  xnew <- matrix(NA, nrow=nrow(start), ncol=ncol(X)+1)
  for(i in 1:nrow(start)) {
    if(EI(gpi, start[i,], fmin) <= eps)
    { out <- list(value=-Inf); next }
    out <- tryCatch( optim(start[i,], obj.EI, method="L-BFGS-B", 
                lower=0, upper=1, gpi=gpi, fmin=fmin), error=function(e){})
    if(!is.null(out)){
      xnew[i,] <- c(out$par, -out$value)
    }
  }
  if(identical(xnew, matrix(NA, nrow=nrow(start), ncol=ncol(X)+1))){
    ## if didn't find xnew with positive EI in the neiborhood, just return something random
    xnew <- cbind(start, 0:(nrow(start)-1))  
  }
  solns <- data.frame(cbind(start, xnew))
  names(solns) <- c("s1", "s2","s3", "x1", "x2", "x3", "val")
  solns <- solns[(solns$val > sqrt(.Machine$double.eps)),]
  return(solns)
}

## apply the EI sequential design scheme with different initial design methods
optim.EI <- function(f, ninit, dim, stop, theta_max, ab = NULL, design)
{
  if(design == "maximin"){
    X <- maximin(ninit, dim)
  } else if(design == "dist_beta"){
    X <- bd(ninit, dim, T = 10^5, shape1 = 2, shape2 = 5)$X
  } else if(design == "LHS"){
    X <- randomLHS(ninit, dim)
  } else if(design == "random"){
    X <- matrix(runif(ninit*dim), ncol = dim)
  }else if(design == "lhsbeta1"){
    X <- lhs_beta_1(ninit, dim, T = 10^5, shape1 = 2, shape2 = 5)$X
  # }else if(design == "lhsbeta2"){
  #   X <- lhs_beta_2(ninit, dim, T = 10^5, shape1 = 3.5, shape2 = 7)$X ## number of total iterations is 2T 
  }
  da <- darg(list(mle=TRUE, max=theta_max, min=sqrt(.Machine$double.eps)), X)
  if(!is.null(ab)) { da$ab <- ab } ## ab = c(0,0)
  y <- f(X)
  gpi <- newGP(X, y, d=0.1, g=1e-7, dK=TRUE)
  mle <- mleGP(gpi, param="d", tmin=da$min, tmax=da$max, ab=da$ab)
  maxei <- c()
  for(i in (ninit+1):stop) {
    
    solns <- EI.search(X, y, gpi)
    lid <- 1
    m <- which.max(solns$val)
    maxei <- c(maxei, solns$val[m])
    xnew <- as.matrix(solns[m,(dim+1):(dim*2)])
    ynew <- f(xnew)
    updateGP(gpi, matrix(xnew, nrow=1), ynew)
    # mle <- mleGP(gpi, param="d", tmin=da$min, tmax=da$max, ab=da$ab)
    X <- rbind(X, xnew); y <- c(y, ynew)
    ## gpi <- newGP(X, y, d=0.1, g=1e-7, dK=TRUE)
    ## mleGP(gpi, param="d", tmin=da$min, tmax=da$max, ab=da$ab)$msg
  }
  deleteGP(gpi)
  
  return(list(X=X, y=y, maxei=maxei))
}


## a wrapper that makes the parallel easier
opt_F <- function(f, scale, n, m, ninit){
  
  theta_max <- 10*sqrt(m)
  ab <- c(0,0)
  prog.ei_1 <- prog.ei_2 <- prog.ei_3 <- prog.ei_4 <- prog.ei_5 <- matrix(NA, nrow=1, ncol=n)
  r <- 1
  formals(f)$scale <- scale 
  
  os <- optim.EI(f,ninit, m, n, theta_max, ab, "dist_beta")
  prog.ei_1[r,1:length(os$y)] <- os$y
  for(i in 2:length(os$y)) { 
    if(is.na(prog.ei_1[r,i]) || prog.ei_1[r,i] > prog.ei_1[r,i-1]) 
      prog.ei_1[r,i] <- prog.ei_1[r,i-1] 
  }
  
  os <- optim.EI(f,ninit, m, n, theta_max, ab, "maximin")
  prog.ei_2[r,1:length(os$y)] <- os$y
  for(i in 2:length(os$y)) { 
    if(is.na(prog.ei_2[r,i]) || prog.ei_2[r,i] > prog.ei_2[r,i-1]) 
      prog.ei_2[r,i] <- prog.ei_2[r,i-1] 
  }
  
  os <- optim.EI(f,ninit, m, n, theta_max, ab, "LHS")
  prog.ei_3[r,1:length(os$y)] <- os$y
  for(i in 2:length(os$y)) { 
    if(is.na(prog.ei_3[r,i]) || prog.ei_3[r,i] > prog.ei_3[r,i-1]) 
      prog.ei_3[r,i] <- prog.ei_3[r,i-1] 
  }
  
  os <- optim.EI(f,ninit, m, n, theta_max, ab,  "random")
  prog.ei_4[r,1:length(os$y)] <- os$y
  for(i in 2:length(os$y)) { 
    if(is.na(prog.ei_4[r,i]) || prog.ei_4[r,i] > prog.ei_4[r,i-1]) 
      prog.ei_4[r,i] <- prog.ei_4[r,i-1] 
  }
  
  os <- optim.EI(f,ninit, m, n, theta_max, ab,  "lhsbeta1")
  prog.ei_5[r,1:length(os$y)] <- os$y
  for(i in 2:length(os$y)) { 
    if(is.na(prog.ei_5[r,i]) || prog.ei_5[r,i] > prog.ei_5[r,i-1]) 
      prog.ei_5[r,i] <- prog.ei_5[r,i-1] 
  }
  
  # os <- optim.EI(f,ninit, m, n, theta_max, ab,  "lhsbeta2")
  # prog.ei_6[r,1:length(os$y)] <- os$y
  # for(i in 2:length(os$y)) { 
  #   if(is.na(prog.ei_6[r,i]) || prog.ei_6[r,i] > prog.ei_6[r,i-1]) 
  #     prog.ei_6[r,i] <- prog.ei_6[r,i-1] 
  # }
  
  output <-cbind(scale = scale, c("dist_beta", "maximin", "LHS", "random", "lhsbeta1"),
                 rbind(prog.ei_1, prog.ei_2, prog.ei_3, prog.ei_4, prog.ei_5))
  return(output)
}


## run the opt_F in parallel
Sys.time()
scales <- runif(1000, 0, 10) ## randomly set the true scale parameter from 0 to 10
set <- expand.grid(scale = scales, n = 50, m = 3, ninit = 16) ## n = 200
colnames(set) <- c("scale", "n", "m", "ninit")
cl <- makeCluster(8)
registerDoParallel(cl)
## the initial Y(X) are the first ninit columns of the out file
out <- foreach(i = 1:nrow(set), .errorhandling = "stop", .combine='rbind', .packages=c('lhs', 'laGP', 'gdata' )) %dopar% { 
  opt_F(f = f, scale = set[i,1], n = set[i,2], m = set[i,3], ninit = set[i,4]) }
stopCluster(cl)
Sys.time()

mms <- as.data.frame(apply(out[,-c(1,2)], 2, as.numeric))
out <- data.frame(design = out[,2], scale = as.numeric(out[,1]), mms)

save.image("optim_ei_3d.RData")

## create p-value table
load("optim_ei_3d.RData")

nstop <-  50 #50
COL <- nstop + 2


pvm <- matrix(rep(NA, 5*5), nrow = 5)
colnames(pvm) <- rownames(pvm) <- c( "maximin", "LHS", "random", "dist_beta", "lhsbeta1")
for( j in 1:5){
  for( i in 1:5)
  { d1 <-  colnames(pvm)[j]
  d2 <-  rownames(pvm)[i]
  sp1 <- out[out$design == d1, COL]
  sp2 <- out[out$design == d2, COL]
  pvm[j,i] <- t.test(log(sp1), log(sp2), alternative = "less", paired = T)$p.value
  }
}
library(xtable)
pvm[pvm>0.99] <- ">0.99"
pvm[as.numeric(pvm)<10^(-7)] <- "<1e-7"
xtable(pvm, digits = 2)

# ## plot progress
# par(mfcol = c(1,1))
# plot(as.numeric(log(colMeans(out[out$design == "maximin",-c(1,2)]))), type = "l", ylim = c(-7,0), ylab = "log(y_min)")
# lines(as.numeric(log(colMeans(out[out$design == "LHS",-c(1,2)]))), col = 2)
# lines(as.numeric(log(colMeans(out[out$design == "random",-c(1,2)]))), col = 3)
# lines(as.numeric(log(colMeans(out[out$design == "dist_beta",-c(1,2)]))), col = 4)
# lines(as.numeric(log(colMeans(out[out$design == "lhsbeta2",-c(1,2)]))), col = 5)
# lines(as.numeric(log(colMeans(out[out$design == "lhsbeta1",-c(1,2)]))), col = 6)
# legend("topright", c( "maximin", "LHS", "random", "dist_beta", "lhsbeta1", "lhsbeta2"), col = c(1:6), lty = c(1,1,1,1,1,1))
