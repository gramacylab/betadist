### This directory contains code supporting the Expected improvement for optimization example
Codes in the directory implement EI optimization initialized by maximin, LHS, beta, lhsbeta1 and lhsbeta2 respectively in parallel; The optimization processes, i.e f_min's at each step, are recorded; Based on those, paired t-test is applied to compare initial designs.

#### The content of each file is listed as follows:  

- `optim_ei_2d.R`: 2d case of EI example **Table 1**.

- `optim_ei_3d.R`: 3d case of EI example **Table 2**.

- `optim_ei_4d.R`: 4d case of EI example **Table 3**.

