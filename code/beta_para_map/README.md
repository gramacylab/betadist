`beta_para.R` comparison of multiple beta shape parameters on mse

`beta_para_map_X_X.R` mse heat maps w.r.t beta shape parameters

`fit_beta.R` fit beta parameters for better random designs distance distributions by `fitdidstr`
