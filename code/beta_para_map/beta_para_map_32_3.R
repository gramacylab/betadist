### This experiment compares intergrated squared error (ISE) of d, d $\in (0,1)^{dim}$, on beta distributions with 100+2 different parameters, dim = 2, n =8
library(plgp)
library(mvtnorm)
library(laGP)

library(foreach)
library(doParallel)
source("ud_new.R")
Sys.time()
gpi.mse <- function(n, dim, shape1, shape2, I = 1000, seed = 0){
  set.seed(seed)
  x <- bd(n=n, m=dim, shape1 = shape1, shape2  = shape2)$X
  D <- distance(x)
  eps <- sqrt(.Machine$double.eps)
  dtrue <- runif(I, min = eps, max = sqrt(dim))
  dhat <- rep(NA, I)
  for(i in 1:I){
    sigma <- exp(-D/dtrue[i] + diag(eps, n))
    y <- rmvnorm(1, sigma = sigma)
    gpi <- newGP(x, y, d = 0.1, g = eps, dK = T)
    dhat[i] <- mleGP(gpi, param = "d")$d
    deleteGP(gpi)
  }
  mse <- mean((dtrue - dhat)^2)
  return(c(n, dim, shape1, shape2, mse, seed))
}


A <- cbind( n = 16, dim = 3, maximin(200,2)*15)
set <- matrix(rep(t(A),100), ncol=ncol(A), byrow=TRUE)
set <- cbind(set, 1:nrow(set))
colnames(set) <- c("n", "dim", "shape1","shape2", "seed")
cl <- makeCluster(8)
registerDoParallel(cl)
out <- foreach(i = 1:nrow(set), .errorhandling = "remove", .combine='rbind', .packages=c('plgp', 'mvtnorm', 'laGP', 'gdata' )) %dopar% { 
  gpi.mse(n = set[i,1], dim = set[i,2], shape1 = set[i,3], shape2 = set[i,4], seed = set[i,5]) }

stopCluster(cl)
save.image('beta_para_map_16_3.RData') 
Sys.time()