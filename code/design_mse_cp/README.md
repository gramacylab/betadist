### This directory contains code supporting the comparison of RMSE of GP lengthscale parameter

`Better_rand_design_plot.R`  to find better random designs with lower mse and plot the densities of their pairwise distance distributions. **Figure3** is generated.

`GP_mse_rand_unifdist_cp.R` to compare multiple designs in terms of RMSE of GP length-scale parameter

`theta_postprocess.R` to generate **Figure1** based on output from `GP_mse_rand_unifdist_cp.R`.

`studentized_rmse_plot.R` to generate **Figure2** based on output from `GP_mse_rand_unifdist_cp.R`.

`gpi_mse_cp.R` compares maximin, random and four unifdist with different parameters settings; not included in the paper.

 
