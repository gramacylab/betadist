## compare rmse of d on unifdist.rand, random design, maximin and beta(3,6)
library(plgp)
library(mvtnorm)
library(laGP)
library(lhs)
library(foreach)
library(doParallel)

source("../design/ud.R")
source("../design/ud_new.R")
source("../design/lhsbeta_design.R")
source("../design/minphi.R")

eps <- sqrt(.Machine$double.eps)
theta_true <- seq(from = 0.1, to = sqrt(dim), length.out = 30)

gpi.mse <- function(method, n = 8, dim = 2, dtrue = theta_true, shape1 = 2, shape2 = 5){
  eps <- sqrt(.Machine$double.eps)
  if(method == 1){
    x <- randomLHS(n, dim)
  }
  else if(method == 2){
    x <- matrix(runif(n*dim), ncol = dim)
  }
  else if(method == 3){
    x <- maximin(n, dim)
  }
  else if(method == 4){
    x <- ud(n, dim)$X
  }
  else if(method == 5){
    x <- bd(n, dim, T = 10^5, shape1 = shape1, shape2 = shape2)$X
  }
  else if(method == 6){
    x <- lhs_beta_1(n, dim, T = 10^5, shape1 = shape1, shape2 = shape2)$X
  }
  else if(method == 7){
    x <- myminphi(n, dim, T = 10^5)$X
  }
  else if(method == 8){
    x <- myminphi(n, dim, p=5, T = 10^5)$X
  }
  D <- distance(x)
  I <- length(dtrue)
  dhat <- rep(NA, I)
  for(i in 1:I){
    sigma <- exp(-D/dtrue[i] + diag(eps, n))
    y <- rmvnorm(1, sigma = sigma)
    gpi <- newGP(x, y, d = 0.1, g = eps, dK = T)
    dhat[i] <- mleGP(gpi, param = "d")$d
    deleteGP(gpi)
  }
  se <- (dtrue - dhat)^2
  return(cbind(n = n, dim = dim, method = method, theta_true = dtrue, se = se))
}

# dim = 2, n = 8-------------------------------------------------
dim = 2
n = 8
formals(gpi.mse)$dim <- dim
formals(gpi.mse)$n <- n

# run gpi.mse in parallel
A <- expand.grid(method = c(1,2,3,4,5,6,7,8))
set <- matrix(rep(t(A),1000), ncol=ncol(A), byrow=TRUE)
colnames(set) <- c("method")
cl <- makeCluster(8)
registerDoParallel(cl)
out <- foreach(i = 1:nrow(set), .errorhandling = "stop", .combine='rbind', .packages=c('plgp', 'mvtnorm', 'laGP', 'lhs', 'gdata' )) %dopar% { 
  gpi.mse(method =set[i,1]) }

stopCluster(cl)
save.image('../../rdata_new/mse_new_n8dim2.RData') 

# dim = 3, n = 16-------------------------------------------------
dim = 3
n = 16
formals(gpi.mse)$dim <- dim
formals(gpi.mse)$n <- n

# run gpi.mse in parallel
A <- expand.grid(method = c(1,2,3,4,5,6,7,8))
set <- matrix(rep(t(A),1000), ncol=ncol(A), byrow=TRUE)
colnames(set) <- c("method")
cl <- makeCluster(8)
registerDoParallel(cl)
out <- foreach(i = 1:nrow(set), .errorhandling = "remove", .combine='rbind', .packages=c('plgp', 'mvtnorm', 'laGP', 'lhs', 'gdata' )) %dopar% { 
  gpi.mse(method =set[i,1]) }

stopCluster(cl)
save.image('../../rdata_new/mse_new_n16dim3.RData')
#save.image('mse_new_n16dim3_1.RData') 

# dim = 4, n = 32-------------------------------------------------
dim = 4
n = 32
formals(gpi.mse)$dim <- dim
formals(gpi.mse)$n <- n

# run gpi.mse in parallel
A <- expand.grid(method = c(1,2,3,4,5,6,7,8))
#A <- expand.grid(method = c(3))
set <- matrix(rep(t(A),1000), ncol=ncol(A), byrow=TRUE)
colnames(set) <- c("method")
cl <- makeCluster(8)
registerDoParallel(cl)
out <- foreach(i = 1:nrow(set), .errorhandling = "remove", .combine='rbind', .packages=c('plgp', 'mvtnorm', 'laGP', 'lhs', 'gdata' )) %dopar% { 
  gpi.mse(method =set[i,1]) }

stopCluster(cl)
save.image('../../rdata_new/mse_new_n32dim4_maximin.RData') 

# dim = 5, n = 64-------------------------------------------------
dim = 5
n = 64
formals(gpi.mse)$dim <- dim
formals(gpi.mse)$n <- n
formals(gpi.mse)$shape2 <- 4.5

# A <- expand.grid(method = c(1,2,3,4,5,6))
A <- expand.grid(method = c(7,8))
set <- matrix(rep(t(A),1000), ncol=ncol(A), byrow=TRUE)
colnames(set) <- c("method")
cl <- makeCluster(8, outfile="")
registerDoParallel(cl)
out <- foreach(i = 1:nrow(set), .errorhandling = "remove", .combine='rbind', .packages=c('plgp', 'mvtnorm', 'laGP', 'lhs', 'gdata' )) %dopar% { 
  gpi.mse(method =set[i,1]) }

stopCluster(cl)
# save.image('../../rdata/mse_new_n64dim5.RData') 
save.image('../../rdata/mse_new_n64dim5_minphi.RData') 


# dim = 6, n = 128-------------------------------------------------
dim = 6
n = 128
formals(gpi.mse)$dim <- dim
formals(gpi.mse)$n <- n
formals(gpi.mse)$shape2 <- 4.5

# A <- expand.grid(method = c(1,2,3,4,5,6))
A <- expand.grid(method = c(7,8))
set <- matrix(rep(t(A),1000), ncol=ncol(A), byrow=TRUE)
colnames(set) <- c("method")
cl <- makeCluster(8, outfile="")
registerDoParallel(cl)
out <- foreach(i = 1:nrow(set), .errorhandling = "remove", .combine='rbind', .packages=c('plgp', 'mvtnorm', 'laGP', 'lhs', 'gdata' )) %dopar% { 
  gpi.mse(method =set[i,1]) }

stopCluster(cl)
# save.image('../../rdata/mse_new_n128dim6.RData') 
save.image('../../rdata/mse_new_n128dim6_minphi.RData') 


