## new objective
load('../../rdata_new/mse_new_n8dim2.RData')
n = 8
dim = 2

gpi.mse <- function(x, n = 8, dim = 2, dtrue = theta_true, reps = 30, detrend_mean = meanf, detrend_sd = s){
  eps <- sqrt(.Machine$double.eps)
  dhat <- matrix(NA, ncol = reps, nrow = length(dtrue))
  for(j in 1:ncol(dhat)){
    D <- distance(x)
    for(i in 1:nrow(dhat)){
      sigma <- exp(-D/dtrue[i] + diag(eps, n))
      y <- rmvnorm(1, sigma = sigma)
      gpi <- newGP(x, y, d = 0.1, g = eps, dK = T)
      dhat[i,j] <- mleGP(gpi, tmin= 0.01, tmax = sqrt(dim), param = "d")$d
      deleteGP(gpi)
    }
  }
  Dtrue <- matrix(rep(dtrue, reps), ncol = reps, byrow = F)
  se <- (Dtrue - dhat)^2
  rmse <- sqrt(rowMeans(matrix(se, ncol = reps, byrow = F)))
  detrend_rmse <- (rmse-detrend_mean)/detrend_sd
  out1 <- mean(detrend_rmse) ## after detrending, design with lower mean is better
  out2 <- sd(detrend_rmse)

  return(out1)
}

# gpi.mse <- function(x, n = 8, dim = 2, dtrue = theta_true){
#   eps <- sqrt(.Machine$double.eps)
#   D <- distance(x)
#   I <- length(dtrue)
#   dhat <- rep(NA, I)
#   for(i in 1:I){
#     sigma <- exp(-D/dtrue[i] + diag(eps, n))
#     y <- rmvnorm(1, sigma = sigma)
#     gpi <- newGP(x, y, d = 0.1, g = eps, dK = T)
#     dhat[i] <- mleGP(gpi, param = "d")$d
#     deleteGP(gpi)
#   }
#   se <- (dtrue - dhat)^2
#   return(cbind(theta_true = dtrue, se = se))
# }

out <- as.data.frame(out)
out <- aggregate(out$se, by = list(out$theta_true, out$method), FUN= mean)

names(out) <- c("theta_true", "method", "MSE")
out <- out[out$method != 8,]
out$method <- replace(out$method, out$method == 1, "lhs" )
out$method <- replace(out$method, out$method == 2, "random" )
out$method <- replace(out$method, out$method == 3, "maximin" )
out$method <- replace(out$method, out$method == 4, "unifdist" )
out$method <- replace(out$method, out$method == 5, "beta")
out$method <- replace(out$method, out$method == 6, "lhsbeta" )
out$method <- replace(out$method, out$method == 7, "minphi2" )
# out$method <- replace(out$method, out$method == 8, "minphi5" )

out$method <- factor(out$method, levels=c("maximin", "minphi2", "lhs", "random", "unifdist", "beta", "lhsbeta"))
out$RMSE <- sqrt(out$MSE)

## detrend to create a baseline
fit <- mleHetTP(out$theta_true, out$RMSE, lower=0.1, upper=30, maxit=1000, covtype="Matern5_2")
summary(fit)
x <- theta_true
p <- predict(fit, matrix(x, ncol=1))
s <- sqrt(p$sd2 + p$nugs)
meanf <- p$mean

## create 10000 random designs
rand_design <- vector(mode = "list", length = 10000)
rmse <- gpi.mse(matrix(runif(n*dim), ncol = dim))
outs <- vector("list", length = 10000)
for(i in 1:10000){
  x <- matrix(runif(n*dim), ncol = dim)
  rand_design[[i]] <- x
 tt <- gpi.mse(x)
  rmse <- c(rmse, tt)
}



# ## detrend to create a baseline
# test <- aggregate(logmse[,2], by = list(logmse[,1], rep(1:5, each = 6060)), FUN= mean)
# plot(test[,1], test[,3])

# fit <- mleHetTP(test[,1], test[,3], lower=0.1, upper=30, maxit=1000, covtype="Matern5_2")
# summary(fit)
# x <- theta_true
# p <- predict(fit, matrix(x, ncol=1))
# s <- sqrt(p$sd2 + p$nugs)
# meanf <- p$mean

# detrend_se <- rep(NA, 1000)
# for(i in 1:1000){
#   se <- outs[[i]][,2]
#   detrend_se[i] <- mean((se - meanf)/s)
# }

detrend_se <-rmse

mse3 <- detrend_se
rand3top50 <- c(1:10000)[rank(mse3)<=50]
d1 <- dist(rand_design[[rand3top50[1]]])
for(i in rand3top50){
  d1 <- c(d1, dist(rand_design[[i]]))
}

rand3end50 <- c(1:10000)[rank(mse3)>=9951]
d5 <- dist(rand_design[[rand3end50[1]]])
for(i in rand3end50){
  d5 <- c(d5, dist(rand_design[[i]]))
}

save.image("rand_design_pattern_detrend.RData")

pdf(file = "../../docs/paper/empirical-density.pdf", width = 0.75*7, height = 0.75*7)
x <- seq(0, sqrt(2),length.out = 100)
y <- dbeta(x/sqrt(2), 2.5, 4)/sqrt(2)
plot(x,y, col = "red", type = "l", lty = 1, lwd = 2, ylim = c(0,3),  main = "",
     ylab = "Empirical Density", xlab = "Pairwise Distance")
# bd_dist <- density(dist(bd(2,8,shape1 = 2, shape2 = 5)$X), from =0, to = sqrt(2))
# plot(bd_dist)
# lines(density(d5, from =0, to = sqrt(2)), col = "grey", lty = 2, lwd = 2)
lines(density(d1, from =0, to = sqrt(2)), col = "black", lty = 3, main = "",
      ylab = "Empirical Density", xlab = "Pairwise Distance", lwd = 2)
lines(density(max_dist, from =0, to = sqrt(2)), col = "blue", lty =  4 , lwd = 2)
# legend("topright", c("betadist(2.5,4)", "1-50",  "9951-10000", "maximin"), col = c("red", "black", "grey", "blue"), lty = c(1,3,2,4), lwd = 2)
legend("topright", c("betadist(2.5,4)", "1-50", "maximin"), col = c("red", "black", "blue"), lty = c(1,3,4), lwd = 2)
dev.off()
