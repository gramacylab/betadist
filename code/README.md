## This directory contains code supporting the project on Distance-distributed design for Gaussian process surrogates

### Below lists what each sub-directory comprises of: 
- `design`  code for implementing designs, including maximin design, unifdist design, beta design, and lhsbeta design

- `design_mse_cp`  code for comparing design methods in terms of MSE of GP length-scale parameter

- `beta_para_map`  code for finding best shape parameter of beta design in certain cases

- `optim_ei` code for comparing multiple design methods under expected improvement based sequential design scheme 

- `alm` code for comparing multiple design methods under ALM based sequential design scheme 
