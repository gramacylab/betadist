library(gdata)

myks_beta <- function(d, f1, dim, shape1, shape2){
  d <- sort(d)/sqrt(dim)
  f2 <- pbeta(d, shape1 = shape1, shape2 = shape2)
  return(max(abs(f2-f1)))
}

## change latin hyper cubes and points at  each iteration
lhs_beta_1 <- function(n, m, T = 10^5, shape1, shape2){
  
  ## generate the Latin hypercube 
  ranperm <- function(X, N) order(runif(N))
  X <- matrix(nrow = n, ncol = m)
  lhcube <- apply(X, 2, ranperm, N = n) - 1
  
  ## draw the random uniforms and turn the hypercube into a sample
  X <- (lhcube + matrix(runif(n * m), nrow = n, ncol = m))/n
  
  ## record current  k-s distance ks0 and distance matrix 
  dX0 <- as.matrix(dist(X, diag = T, upper = T))
  f0 <- cumsum(rep(1,n*(n-1)/2))/(n*(n-1)/2)
  ks0 <- myks_beta(upperTriangle(dX0), f0, dim=m, shape1, shape2) ## k-s test
  dprime <- dX0
  k <- 1
  prog <- rep(NA, T)
  for(t in 1:T) {
    lhcube_old <- lhcube
    Xold <- X
    rand_row <- sample(1:n, 2)
    temp <- lhcube[rand_row[1],1]
    lhcube[rand_row[1],1] <-  lhcube[rand_row[2],1]
    lhcube[rand_row[2],1] <- temp
    X[rand_row[1],] <- (lhcube[rand_row[1],] + runif(m))/n
    X[rand_row[2],] <- (lhcube[rand_row[2],] + runif(m))/n
    dX <- as.matrix(dist(X, diag = T, upper = T))
    ksprime <- myks_beta(upperTriangle(dX), f0, dim=m, shape1, shape2) ## k-s test
    if(ksprime < ks0) { ks0 <- ksprime  ## accept
    } else { X <- Xold; lhcube <- lhcube_old}         ## reject
    prog[t] <- ks0
  }
  
  ## return the design
  return(list(X = X, ksopt = ks0, prog = prog))
}

## change latin hyper cubes, then change points
lhs_beta_2 <- function(n, m, T = 10^5, shape1, shape2){
  
  ## generate the Latin hypercube 
  ranperm <- function(X, N) order(runif(N))
  X <- matrix(nrow = n, ncol = m)
  lhcube <- apply(X, 2, ranperm, N = n) - 1
  f0 <- cumsum(rep(1,n*(n-1)/2))/(n*(n-1)/2)
  ksc <- myks_beta(upperTriangle(as.matrix(dist(lhcube/n, diag = T, upper = T))), f0, dim=m, shape1, shape2) ## k-s test
  
  ## change latin hyper cubes
  for(t in 1:T) {
    lhcube_old <- lhcube
    rand_row <- sample(1:n, 2)
    temp <- lhcube[rand_row[1],1]
    lhcube[rand_row[1],1] <-  lhcube[rand_row[2],1]
    lhcube[rand_row[2],1] <- temp
    dc <- as.matrix(dist(lhcube/n, diag = T, upper = T))
    ksprime_c <- myks_beta(upperTriangle(dc), f0, dim=m, shape1, shape2) ## k-s test
    if(ksprime_c < ksc) { ksc <- ksprime_c  ## accept
    } else {lhcube <- lhcube_old}         ## reject
  }
  
  ## draw the random uniforms and turn the hypercube into a sample
  X <- (lhcube + matrix(runif(n * m), nrow = n, ncol = m))/n
  
  ## record current  k-s distance ks0 and distance matrix 
  dX0 <- as.matrix(dist(X, diag = T, upper = T))
  f0 <- cumsum(rep(1,n*(n-1)/2))/(n*(n-1)/2)
  ks0 <- myks_beta(upperTriangle(dX0), f0, dim=m, shape1, shape2) ## k-s test
  dprime <- dX0
  
  ## then change points within cubes
  for(t in 1:T) {
    Xold <- X
    rownum <- sample(1:n, 1)
    X[rownum,] <- (lhcube[rownum,] + runif(m))/n
    dX <- as.matrix(dist(X, diag = T, upper = T))
    ksprime <- myks_beta(upperTriangle(dX), f0, dim=m, shape1, shape2) ## k-s test
    if(ksprime < ks0) { ks0 <- ksprime  ## accept
    } else { X <- Xold }         ## reject
  }
  
  ## return the design
  return(list(X = X, ksopt = ks0))
}


# n = 8
# # f0 <- cumsum(rep(1,n*(n-1)/2))/(n*(n-1)/2)
# out1 <- rep(NA, 10)
# out2 <- rep(NA, 10)
# out3 <- rep(NA, 10)
# for( i in 1:10 ){
#   out1[i] <- lhs_beta_1(n,3,10^5, 2,5)$ksopt
#   out2[i] <- lhs_beta_2(n,3,10^5/2, 2,5)$ksopt
#   out3[i] <- bd(n,3,10^5, 2,5)$ksopt
# }
# 
# boxplot(out1, out2, out3, names = c("lhs_beta1", "lhs_beta2", "bd"), ylab = "k-s distance to beta(2,5)")
