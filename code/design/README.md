### This directory contains code of designs
Code in the directory implements designs, including maximin design, unifdist design, betadist design, and lhsbeta design.

#### The content of each file is listed as follows:  
`maximin.R` code for maximin design; main function is `maximin`

`ud.R` code for unifdist design; main function is `ud`

`ud_new.R` code for unifdist and betadist designs; main function is `bd`

`lhsbeta_design.R` lhsbeta design with two types of implementation; main functions are `lhsbeta1` and `lhsbeta2`

`design_visual.R` code for visualize design in 2d and 3d; **Figure6**
