`maximin.R` maximin( ): create maximin design

`ud.R`

* ud( ): create unifdist design with random initial
* ud.1( ): create unifdist design with maximin initial
* ud.11( ): create unifdist design with maximin initial and an arbitrary stopping conditon of k-s distance 
* edfPlot( ): plot ecdf and histogram of distance distribution. Input is output of ud, ud.1 or ud.11.
* designPlot( ): plot design points from a 1-d or 2-d projection.
* ksPlot( ): plot k-s distance convergence curve of an ud object.
 

`testk.R` Explore relationship of ksopt, final optimized k-s distance, and k, number of sample updated at each iteration.

`testn.R` Explore relationship of ksopt and n, sample size.

`GP-1d-1.R` Length scale parameter d simulation comparing unifdist and maximin designs. Unifdist: n=10, m = 1, k = 1, T = 10^5, dmax = 0.9,1. maximin: n=10, m=1. dtrue is in (0,1). Replicates = 30.

`GP-1d.R` rmse simulation with friedman function as the truth. Compares unifdist and maximin design.

`test2ds.R` Compares ud( )(unifdist with random initial) and ud.1( )(unifdist with maximin initial) in terms of final optimized k-s distance. 