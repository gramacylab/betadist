### try to find relationship of ksopt and n

source("ud.R")
ud.ksopt.1<- function(x){
  n <- x[1]
  m <- x[2]
  k <- x[3]
  T <- x[4]
  dmax <- x[5]
  ksopt <- ud.1(n,m,k,T,dmax)$ksopt
  return(cbind(n = n, m = m, k = k, ksopt = ksopt))
}

ud.ksopt<- function(x){
  n <- x[1]
  m <- x[2]
  k <- x[3]
  T <- x[4]
  dmax <- x[5]
  ksopt <- ud(n,m,k,T,dmax)$ksopt
  return(cbind(n = n, m = m, k = k, ksopt = ksopt))
}

A <- expand.grid(n = c(10,20,30,40,50,60,70,100), m = c(1,2,3), k = 1, T = 10^5, dmax = 1)

set <- matrix(rep(t(A),30), ncol=ncol(A), byrow=TRUE)


library(foreach)
library(doParallel)

cl <- makeCluster(8)
registerDoParallel(cl)
out2 <- foreach(i = 1:nrow(set), .combine='rbind', .packages='plgp') %dopar% {ud.ksopt.1(as.vector(set[i,]))}
stopCluster(cl)
save.image('testn.RData') 