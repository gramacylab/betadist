library(plgp)
library(mvtnorm)
library(laGP)

library(foreach)
library(doParallel)

source('ud.R')

gpi.mse <- function(method, n, dim, I){
	if(method ==3){
		x <- ud(n,dim,1,10^5,1)$X
	}
	else if(method == 2){
		x <- maximin(n,dim)
	}
	else if(method == 1){
			x <- matrix(runif(n*dim), ncol = dim)
	}
  else if(method ==4){
    x <- ud.1(n,dim,1,10^5,1)$X
  }
  else if(method ==5){
    x <- ud(n,dim,1,10^5,0.9)$X
  }
	else {stop("wrong method")}
	
	D <- distance(x)
	eps <- sqrt(.Machine$double.eps)
	dtrue <- runif(I)
	dhat <- rep(NA, I)
	for(i in 1:I){
		sigma <- exp(-D/dtrue[i] + diag(eps, n))
		y <- rmvnorm(1, sigma = sigma)
		gpi <- newGP(x, y, d = 0.1, g = eps, dK = T)
		dhat[i] <- mleGP(gpi, param = "d", tmax = 10)$d
		deleteGP(gpi)
	}
	mse <- mean((dtrue - dhat)^2)
	return(c(n, dim, method, mse))
}


A <- expand.grid(method = c(1,2,3,4,5), dim = c(2,3,4))
set <- matrix(rep(t(A),30), ncol=ncol(A), byrow=TRUE)
colnames(set) <- c("method", "dim")
cl <- makeCluster(8)
registerDoParallel(cl)
out <- foreach(i = 1:nrow(set), .combine='rbind', .packages=c('plgp', 'mvtnorm', 'laGP' )) %dopar% { gpi.mse(set[i], 8, set[i,2], 1000) }

stopCluster(cl)
save.image('GP_234d_mse.RData') 

