library(laGP)
source('ud.R')
## true surface with input x from 0 to 1
f <- function(x) (sin(pi*10*x/5) + 0.2*cos(4*pi*10*x/5)) + rnorm(1, sd=0.2)

## return the rmse, with input design x and y(x)
gpi.rmse <- function(x, f){
  if(class(x)!="matrix"){ stop("x must be a matrix")}
  y <- do.call('f', list(x))
  gpi <- newGP(x, y, d = 0.1, g = 0.2, dK = T) 
  mle <- mleGP(gpi, param = "d",  tmax = max(dist(x)))
  xx <- matrix(seq(0, 1, length=40), ncol = 1)
  yytrue <- do.call('f', list(xx))
  p <- predGP(gpi, xx)
  deleteGP(gpi)
  rmse <- sqrt(mean((yytrue - p$mean)^2))
  return(rmse)
}

# 1-d with 30 replicates 
R <- 10
ud.rmse <- rep(NA, R)
ud.rmse1 <- rep(NA, R)
maximin.rmse <- rep(NA, R)
for( i in 1:R){
  x <- ud.11(10,1,1,0.9)$X
  ud.rmse[i] <- gpi.rmse(x,f)
  x <- ud.11(10,1,1,1)$X
  ud.rmse1[i] <- gpi.rmse(x,f)
  x <- maximin(10,1)
  maximin.rmse[i] <- gpi.rmse(x,f)
}

save.image('GP-1d.RData') 

