### This directory contains code supporting the Active Learning MacKay example
Active learning MacKay(ALM) is a method of sequential design for reducing predictive uncertainty. We compared the performance of LHS, maximin, beta and lhsbeta as initial designs, in terms of RMSPE under ALM sequential design scheme.

#### The content of each file is listed as follows:  

- `alm_example_prl.R`: to implement ALM method and calculate RMSPE from ALM initialized by maximin, LHS, beta, and lhsbeta seperately in parallel; **Figure 7**.


