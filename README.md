# betadist: Beta Distributed Distance Designs for Gaussian Processes #

This repository contains source code and documentation for a project exploring the potential for uniform distance designs to provide more for more efficient estimation of Gaussian process lengthscale parameters.

### First, some reading ###

Since this project is just starting out, the first task is to get up to speed on GP modeling and design.

* Webinar on GPs: https://www.youtube.com/watch?v=XxqVPzb_sGM
* The content from the Webinar can be downloaded here:  http://bobby.gramacy.com/teaching/gpwebinar/
* For a lecture on design for GPs, see: http://bobby.gramacy.com/teaching/llnl_rsm/lect3_doc.html

### Relevant Papers ###

- Max D. Morris (1991) On counting the number of data pairs for semivariogram estimation. Mathematical Geology, Vol. 25, No. 7, pp 929-943
- C. Devon Lin, Derek Bingham, Randy R. Sitter and Boxin Tang (2010) A new and flexible method for constructing designs for computer experiments. The Annals of Statistics Vol. 38, No. 3, pp. 1460-1477
- Xia, G., Mirand, M. and Gelfand, A. (2006) Approximately optimal spatial design approaches for environmental data. Envirometrics, 17, 363�385.
- Zimmerman, D. (2006) Optimal network design for spatial prediction, covariance parameter estimation and empirical prediction. Envirometrics, 17, 635�652.
- Zhu, Z. and Stein, M. (2005) Spatial sampling design for parameter estimation of the covariance function. Journal of Statistical Planning and Inference, 134, 583�603

### Directory Structure ###

Soon we will create a directory structre as follows

* docs is designed to contain tex, md, and Rmd files, in appropriately labeled sub directories
* code would contain R and C code and examples
* data will contain data files and data generation code

### Who do I talk to? ###

* The project was originated and is actively maintained by Robert B. Gramacy <rbg@vt.edu>
* Contributors include Boya Zhang

### Forget me not ###

* Focus on writing the paper
* run mse_cp simulation with LHS included
